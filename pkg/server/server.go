// Copyright © 2017 The Qaclana Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package server

import (
	"net/http"
	"fmt"
	"log"
	flag "github.com/spf13/pflag"
	"os"
	"syscall"
	"os/signal"
)

func Start(flags *flag.FlagSet) {
	log.Print("Starting Qaclana Proxy")

	var serverChannel = make(chan os.Signal, 0)
	signal.Notify(serverChannel, os.Interrupt, syscall.SIGTERM)

	go func() {
		mainServerMux := http.NewServeMux()
		mainServerMux.HandleFunc("/", handler)
		port, _ := flags.GetInt("port")
		address := fmt.Sprintf("0.0.0.0:%d", port)
		log.Printf("Started Proxy at %s", address)
		log.Fatal(http.ListenAndServe(address, mainServerMux))
	}()
	go func() {
		healthCheckMux := http.NewServeMux()
		healthCheckMux.HandleFunc("/", healthCheckHandler)
		port, _ := flags.GetInt("healthcheck-port")
		address := fmt.Sprintf("0.0.0.0:%d", port)
		log.Printf("Started Health Check Server at %s", address)
		log.Fatal(http.ListenAndServe(address, healthCheckMux))
	}()

	select {
	case <-serverChannel:
		log.Println("Qaclana Proxy is finishing")
	}
}

func handler(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintln(w, "OK")
}

func healthCheckHandler(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintln(w, "OK")
}
