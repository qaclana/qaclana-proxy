FROM centos:7
EXPOSE 8000 9000

COPY _output/qaclana-proxy /qaclana/

CMD ["/qaclana/qaclana-proxy"]
